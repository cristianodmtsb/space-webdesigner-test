import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }
  html, body, #root {
    height: 100%;
  }
  body{
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;
    @import url('https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap');
    font-family: 'Poppins', sans-serif !important;
    color: #fff;
  }
  button{cursor: pointer;}
  h1{
    font-family: 'Poppins', sans-serif;
    font-weight:700;
  }
  a{
    color:#fff !important;
  }
`;
