import React, { Component } from "react";
import TopHomepage from "../../components/TopHomePage";
import ContentHome from "../../components/ContentHome";
import Footer from "../../components/Footer";

// import { Container } from './styles';

export default class Homepage extends Component {
  render() {
    return (
      <>
        <TopHomepage />
        <ContentHome />
        <Footer />
      </>
    );
  }
}
