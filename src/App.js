import React from "react";
import Homepage from "./pages/Homepage";
import { GlobalStyle } from "./styles/global";

function App() {
  return (
    <>
      <GlobalStyle />
      <Homepage />
    </>
  );
}

export default App;
