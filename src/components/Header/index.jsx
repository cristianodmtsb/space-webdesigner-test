import React, { Component } from "react";
import {
  Container,
  Collapse,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";

import JoyJetLogo from "../../assets/images/ic_joyjetnm.svg";
import MenuIcon from "../../assets/images/menu-icon.svg";
import CloseIcon from "../../assets/images/close-icon.svg";

import { MobileMenu, MenuButton, TopMobileMenu } from "./styles";

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <>
        <Container>
          <Navbar color="faded" dark expand="md">
            <NavbarBrand href="/">
              <img src={JoyJetLogo} alt="JoyJet" />
            </NavbarBrand>
            <MenuButton onClick={this.toggle} className="d-block d-sm-none">
              <img src={MenuIcon} alt="Menu" />
            </MenuButton>
            <Nav className="ml-auto text-uppercase d-none d-sm-flex" navbar>
              <NavItem>
                <NavLink href="/">Blog</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/">Popular</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/">Archive</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/">About</NavLink>
              </NavItem>
            </Nav>
          </Navbar>
        </Container>

        <Collapse isOpen={this.state.isOpen} navbar>
          <MobileMenu>
            <TopMobileMenu>
              <img src={JoyJetLogo} alt="Joy Jet" className="logo" />
              <MenuButton className="close-icon" onClick={this.toggle}>
                <img src={CloseIcon} alt="Close menu" />
              </MenuButton>
            </TopMobileMenu>
            <Nav className="ml-auto text-uppercase" navbar>
              <NavItem>
                <NavLink href="/">Blog</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/">Popular</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/">Archive</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/">About</NavLink>
              </NavItem>
            </Nav>
          </MobileMenu>
        </Collapse>
      </>
    );
  }
}
