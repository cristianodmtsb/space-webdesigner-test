import styled from "styled-components";

export const HeaderNav = styled.div``;
export const MenuButton = styled.button`
  background: transparent !important;
  border: none;
  color: #fff;
  img {
    width: 45px;
    height: 45px;
  }
`;
export const MobileMenu = styled.div`
  position: fixed;
  background-color: #4a90e2;
  width: 100%;
  height: 100vh;
  z-index: 99;
  top: 0;
  left: 0;
  .navbar-nav {
    text-align: center;
    a {
      color: #fff;
      line-height: 4em;
    }
  }
`;
export const TopMobileMenu = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  align-items: center;
  padding: 1.8em 2em;
`;
