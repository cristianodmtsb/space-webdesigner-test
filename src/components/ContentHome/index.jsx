import React, { Component } from "react";
import Header from "../Header";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle
} from "reactstrap";

import Thumb1 from "../../assets/images/43688.jpg";
import Thumb2 from "../../assets/images/43690.jpg";
import Thumb3 from "../../assets/images/43698.jpg";

import { NavContent, Content, ContentHomeBase } from "./styles";

const cards = [
  {
    img: Thumb1,
    title: "International Space Station",
    excerpt:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Excepturi, quo ex? Unde nam ab vero placeat magni adipisci aliquid necessitatibus voluptatibus!"
  },
  {
    img: Thumb2,
    title: "International Space Station",
    excerpt:
      "Lorem, unde vitae possimus pariatur aperiam iure dolorum voluptas. Unde nam ab vero placeat magni adipisci aliquid necessitatibus voluptatibus!"
  },
  {
    img: Thumb3,
    title: "International Space Station",
    excerpt:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Excepturi, quo ex? Iste, unde vitae possimus pariatur aperiam iure dolorum voluptas."
  }
];

export default class ContentHome extends Component {
  render() {
    return (
      <ContentHomeBase>
        <NavContent className="d-none d-sm-block">
          <Header />
        </NavContent>
        <Content>
          <Container className="py-sm-3 py-2">
            <Row className="py-5 cardsList">
              {cards.map((card, i) => (
                <Col key={i} sm="4" className="py-2">
                  <Card>
                    <CardImg
                      top
                      width="100%"
                      src={card.img}
                      alt="Card image cap"
                    />
                    <CardBody>
                      <CardTitle>{card.title}</CardTitle>
                      <CardText>{card.excerpt}</CardText>
                    </CardBody>
                  </Card>
                </Col>
              ))}
            </Row>
            <Row className="py-sm-3 py-2 about">
              <Col sm="4" className="py-2 py-sm-0">
                <img
                  src={Thumb1}
                  alt="About"
                  width="100%"
                  className="rounded"
                />
              </Col>
              <Col sm="8">
                <h3>About Us</h3>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Corrupti quisquam excepturi laudantium error ipsam nesciunt,
                  nihil qui magnam nam recusandae laboriosam labore accusamus
                  dolorum in iusto optio temporibus, obcaecati voluptate.
                </p>
                <p>
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Autem, quae non cum esse dolorum veniam dolorem quibusdam
                  suscipit nam possimus odio veritatis. At fugiat nostrum
                  dolorem esse obcaecati? Quam, voluptatum.
                </p>
              </Col>
            </Row>
          </Container>
        </Content>
      </ContentHomeBase>
    );
  }
}
