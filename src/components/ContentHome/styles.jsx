import styled from "styled-components";

export const ContentHomeBase = styled.div`
  position: relative;
`;
export const NavContent = styled.div`
  position: sticky;
  background-color: #4a90e2;
  nav {
    a {
      font-size: 0.875em !important;
      padding: 0.5em 1.5em !important;
    }
  }
`;

export const Content = styled.div`
  background-color: #f2f2f2;
  h3 {
    font-size: #4a90e2;
    text-transform: uppercase;
    font-size: 1.625em;
    color: #4a90e2;
  }
  .cardsList {
    align-items: stretch;
  }
  .card-title {
    font-weight: 700;
  }
  .about {
    align-items: center;
    p {
      line-height: 2em;
    }
  }
`;
