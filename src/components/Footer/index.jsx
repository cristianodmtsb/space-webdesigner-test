import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";

import { FooterContent } from "./styles";

export default class Footer extends Component {
  render() {
    return (
      <FooterContent>
        <Container>
          <Row className="py-2">
            <Col>© 2016 Created by Joyjet Digital Space Agency</Col>
          </Row>
        </Container>
      </FooterContent>
    );
  }
}
