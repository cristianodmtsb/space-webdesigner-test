import styled from "styled-components";

export const FooterContent = styled.div`
  background-color: #f2f2f2;
  color: #908c8c;
  font-size: 0.6875em;
  .row {
    border-top: 1px solid #d8d8d8;
  }
`;
