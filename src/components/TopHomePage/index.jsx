import React, { Component } from "react";
import { Container, Row, Col, Button } from "reactstrap";
import Header from "../Header";
// import { styles } from "ansi-colors";

import { ContainerTop, BottomTop } from "./styles";

export default class TopHomePage extends Component {
  render() {
    return (
      <ContainerTop>
        <Header />
        <Container>
          <Row className="py-3">
            <Col sm="6">
              <h1>
                SPACE<span>.</span>
              </h1>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus
                consectetur dolores illo facere...
              </p>
              <Button color="primary" size="lg">
                click
              </Button>
            </Col>
          </Row>
        </Container>
        <BottomTop className="d-none d-sm-block">
          <Container>
            <Row>
              <Col xs="3">
                <h2>
                  Treding
                  <span>Today</span>
                </h2>
              </Col>
              <Col xs="3">
                Lorem ipsum dolor sit amet, consectetuer adipiscing ligula eget
                dolor.
              </Col>
              <Col xs="3">
                Lorem ipsum dolor sit amet, consectetuer adipiscing ligula eget
                dolor.
              </Col>
              <Col xs="3">
                Lorem ipsum dolor sit amet, consectetuer adipiscing ligula eget
                dolor.
              </Col>
            </Row>
          </Container>
        </BottomTop>
      </ContainerTop>
    );
  }
}
