import styled from "styled-components";
import BackTop from "../../assets/images/43688.jpg";

export const ContainerTop = styled.div`
  height: 100vh;
  min-height: 420px;
  padding-top: 1.2em;
  /* width: 100%; */
  display: block;
  position: relative;
  background-color: #000;
  display: flex;
  justify-content: space-between;
  flex-direction: column;

  &:after {
    top: 0;
    left: 0;
    z-index: 1;
    position: absolute;
    background-image: url(${BackTop});
    background-size: cover;
    background-position: 50% 50%;
    height: 100vh;
    min-height: 420px;
    width: 100%;
    content: "";
    opacity: 0.7;
  }
  h1 {
    color: #fff;
    font-size: 7.375em;
    font-weight: 700;
    padding-bottom: 0;
    margin-bottom: 0;
    span {
      color: #4a90e2;
    }
  }
  p {
    color: #fff;
    letter-spacing: 0.31px;
    line-height: 1.5em;
  }
  button {
    width: 7em;
    background-color: #4a90e2;
    border-color: #4a90e2;
  }
  nav {
    z-index: 2;
    a {
      font-size: 0.875em !important;
      padding: 0.5em 1.5em !important;
    }
  }
  .container {
    z-index: 2;
    position: relative;
  }
  @media (max-width: 576px) {
    height: 80vh;
    &:after {
      height: 80vh;
    }
    h1 {
      font-size: 3em;
    }
    .container {
      .row {
        margin-bottom: 12vh;
      }
    }
  }
`;

export const BottomTop = styled.div`
  width: 100%;
  background-color: rgba(0, 0, 0, 0.3);
  display: flex;
  z-index: 2;
  padding: 1.5em;
  color: #fff;
  .row {
    align-items: center;
    .col-3 {
      border-left: 1px solid #fff;
      line-height: 2em;
      &:first-child {
        border-left: none;
      }
    }
  }
  h2 {
    font-size: 1.25em;
    text-align: right;
    span {
      display: block;
      color: #4a90e2;
    }
  }
`;
